amplitude2(90,8)=0;
latency2(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=atarget2(m:n,x);
[answer,wanted]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude2(j,x-1)=max;
latency2(j,x-1)=(wanted(k)*4)+250;
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end

amplitude8(90,8)=0;
latency8(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=anontarget8(m:n,x);
[answer,wanted]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude8(j,x-1)=max;
latency8(j,x-1)=(wanted(k)*4)+250;
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end

amplitude=vertcat(amplitude2,amplitude8);
latency=vertcat(latency2,latency8);
solution(180,16)=0;
x=1;
while x<=16
    if rem(x,2)==0
        solution(:,x)=latency(:,x/2);
    elseif rem(x,2)~=0
        solution(:,x)=amplitude(:,(x+1)/2);
    end
    x=x+1;
end
csvwrite('Features_Arbitary_Range_Gate.csv',solution);