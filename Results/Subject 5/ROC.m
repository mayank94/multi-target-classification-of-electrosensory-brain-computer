se=vertcat(score1as,score2as);
we=vertcat(score1aw,score2aw);
m(1:360)=0;
m(1:90)=1;
m(271:360)=1;
[xs,ys,ts,aucs]=perfcurve(m,se(:,2),'1');
[xw,yw,tw,aucw]=perfcurve(m,we(:,2),'1');
plot(xs,ys,xw,yw);
xlabel('False Positive Rate');ylabel('True Positive Rate');legend('Single Discriminant','Window Discriminant');title('ROC Curve Subject 5');