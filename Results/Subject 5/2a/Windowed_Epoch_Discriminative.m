j=1;tp=0;tn=0;fp=0;fn=0;m(180,16)=0;
while j<=15
    i=1;
while i<=80
    a=mean(FeaturesArbitaryRangeGate(i:i+10,j));
    b=mean(FeaturesArbitaryRangeGate(i+90:i+100,j));

    e=mean(FeaturesArbitaryRangeGate(i:i+10,j+1));
    f=mean(FeaturesArbitaryRangeGate(i+90:i+100,j+1));

    m(i,j)=a;
    m(i+90,j)=b;
    
    m(i,j+1)=e;
    m(i+90,j+1)=f;

    i=i+1;
end
j=j+2;
end
z(180)=0;
z(91:180)=1;
clas=fitcdiscr(m,z);
[x,score]=predict(clas,m);
z=z.';
i=1;
while i<=180
    if x(i)==0 && z(i)==0
        tn=tn+1;
    elseif x(i)==1 && z(i)==0
        fp=fp+1;
    elseif x(i)==0 && z(i)==1
        fn=fn+1;
    elseif x(i)==1 && z(i)==1
        tp=tp+1;
    end
    i=i+1;
end
percentage=(tp+tn)/(tp+tn+fp+fn)*100;