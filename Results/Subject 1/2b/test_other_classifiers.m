tp=0;tn=0;fp=0;fn=0;
m(360)=0;
m(271:360)=1;
clas=fitcsvm(FeaturesArbitaryRangeGate,m,'KernelFunction','rbf');
[x,score]=predict(clas,FeaturesArbitaryRangeGate);
m=m.';
i=1;
while i<=360
    if x(i)==0 && m(i)==0
        tn=tn+1;
    elseif x(i)==1 && m(i)==0
        fp=fp+1;
    elseif x(i)==0 && m(i)==1
        fn=fn+1;
    elseif x(i)==1 && m(i)==1
        tp=tp+1;
    end
    i=i+1;
end
percentage=(tp+tn)/(tp+tn+fp+fn)*100;