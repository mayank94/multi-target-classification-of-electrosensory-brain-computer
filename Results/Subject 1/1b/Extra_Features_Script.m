amplitude2(90,8)=0;
latency2(90,8)=0;
width2(90,8)=0;
prominence2(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=bnontarget2(m:n,x);
[answer,wanted,width,prom]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude2(j,x-1)=max;
latency2(j,x-1)=(wanted(k)*4)+250;
width2(j,x-1)=width(k);
prominence2(j,x-1)=prom(k);
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end
amplitude4(90,8)=0;
latency4(90,8)=0;
width4(90,8)=0;
prominence4(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=btarget4(m:n,x);
[answer,wanted,width,prom]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude4(j,x-1)=max;
latency4(j,x-1)=(wanted(k)*4)+250;
width4(j,x-1)=width(k);
prominence4(j,x-1)=prom(k);
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end
amplitude8(90,8)=0;
latency8(90,8)=0;
width8(90,8)=0;
prominence8(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=bnontarget8(m:n,x);
[answer,wanted,width,prom]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude8(j,x-1)=max;
latency8(j,x-1)=(wanted(k)*4)+250;
width8(j,x-1)=width(k);
prominence8(j,x-1)=prom(k);
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end
amplitude16(90,8)=0;
latency16(90,8)=0;
width16(90,8)=0;
prominence16(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=bnontarget16(m:n,x);
[answer,wanted,width,prom]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude16(j,x-1)=max;
latency16(j,x-1)=(wanted(k)*4)+250;
width16(j,x-1)=width(k);
prominence16(j,x-1)=prom(k);
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end
amplitude=vertcat(amplitude2,amplitude4,amplitude8,amplitude16);
latency=vertcat(latency2,latency4,latency8,latency16);
width=vertcat(width2,width4,width8,width16);
prominence=vertcat(prominence2,prominence4,prominence8,prominence16);
solution(360,32)=0;
x=1;
while x<=32
    if rem(x,4)==0
        solution(:,x)=prominence(:,x/4);
    elseif rem(x,4)==3
        solution(:,x)=width(:,(x+1)/4);
    elseif rem(x,4)==2
        solution(:,x)=latency(:,(x+2)/4);
    elseif rem(x,4)==1
        solution(:,x)=amplitude(:,(x+3)/4);
    end
    x=x+1;
end
csvwrite('Extra_Features.csv',solution);