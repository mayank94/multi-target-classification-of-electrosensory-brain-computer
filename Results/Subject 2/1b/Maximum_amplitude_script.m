amplitude2(90,8)=0;
latency2(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=bnontarget2(m:n,x);
[answer,wanted]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude2(j,x-1)=max;
latency2(j,x-1)=(wanted(k)*4)+250;
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end
amplitude4(90,8)=0;
latency4(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=btarget4(m:n,x);
[answer,wanted]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude4(j,x-1)=max;
latency4(j,x-1)=(wanted(k)*4)+250;
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end
amplitude8(90,8)=0;
latency8(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=bnontarget8(m:n,x);
[answer,wanted]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude8(j,x-1)=max;
latency8(j,x-1)=(wanted(k)*4)+250;
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end
amplitude16(90,8)=0;
latency16(90,8)=0;
x=2;
while x<=9
   j=1;
   m=127;
   n=188;
while j<=90
z=bnontarget16(m:n,x);
[answer,wanted]=findpeaks(z);
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude16(j,x-1)=max;
latency16(j,x-1)=(wanted(k)*4)+250;
m=m+188;
n=n+188;
j=j+1;
end
x=x+1;
end
amplitude=vertcat(amplitude2,amplitude4,amplitude8,amplitude16);
latency=vertcat(latency2,latency4,latency8,latency16);
solution(360,16)=0;
x=1;
while x<=16
    if rem(x,2)==0
        solution(:,x)=latency(:,x/2);
    elseif rem(x,2)~=0
        solution(:,x)=amplitude(:,(x+1)/2);
    end
    x=x+1;
end
csvwrite('Features_Arbitary_Range_Gate.csv',solution);