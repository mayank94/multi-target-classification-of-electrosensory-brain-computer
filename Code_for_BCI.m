% The preprocessing was carried out in EEGLAB so no code is present

%Feature extraction

amplitude =[];
latency =[];
x=1;
while x<=8 %Loop for all 8 electrodes
   j=1;
   m=127; %Range gate from 250-500ms after stimulus onset
   n=188;
while j<=90
z=atarget2(m:n,x);
[answer,wanted]=findpeaks(z); %Find the peaks
i=1;k=1;
max=answer(1);
d=size(answer);
while i<=d(1,1) %Find highest peak (P300)
    if answer(i)>=max
        max=answer(i);
        k=i;
    end
    i=i+1;
end
amplitude(j,x-1)=max; %Record amplitude
latency(j,x-1)=(wanted(k)*4)+250; %Record latency in ms
m=m+188;%Go to next range gate
n=n+188;
j=j+1;
end
x=x+1;
end

%Post-processing and classification

for j = 1:size(amplitude) %Compute mean of electrodes
    amplitude_mean(j) = mean(amplitude(j,1:8));
    latency_mean(j) = mean(latency(j,1:8));
end 

i=1;
while i<=80 %Take mean of sliding Window
    a=mean(amplitude_mean(i:i+10));
    b=mean(amplitude_mean(i+90:i+100));
    c=mean(amplitude_mean(i+180:i+190));
    d=mean(amplitude_mean(i+270:i+280));
    e=mean(latency_mean(i:i+10));
    f=mean(latency_mean(i+90:i+100));
    g=mean(latency_mean(i+180:i+190));
    h=mean(latency_mean(i+270:i+280));
    m(i,1)=a; %Store averaged values in array
    m(i+90,1)=b;
    m(i+180,1)=c;
    m(i+270,1)=d;
    m(i,2)=e;
    m(i+90,2)=f;
    m(i+180,2)=g;
    m(i+270,2)=h;
    i=i+1;
end

%m can be paired with appropriate labels to create a classifier