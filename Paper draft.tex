\documentclass[twoside]{article}


% ------
% Fonts and typesetting settings
\usepackage[sc]{mathpazo}
\usepackage[T1]{fontenc}
\linespread{1.05} % Palatino needs more space between lines
\usepackage{microtype}

\usepackage{amsmath}
\usepackage{graphicx}


% ------
% Page layout
\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt]{geometry}
\usepackage[font=it]{caption}
\usepackage{paralist}
\usepackage{multicol}

% ------
% Lettrines
\usepackage{lettrine}


% ------
% Abstract
\usepackage{abstract}
	\renewcommand{\abstractnamefont}{\normalfont\bfseries}
	\renewcommand{\abstracttextfont}{\normalfont\small\itshape}


% ------
% Titling (section/subsection)
\usepackage{titlesec}
\renewcommand\thesection{\Roman{section}}
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{}


% ------
% Header/footer
\usepackage{fancyhdr}
	\pagestyle{fancy}
	\fancyhead{}
	\fancyfoot{}
%	\fancyhead[C]{Journal paper template $\bullet$ April 2012 $\bullet$ Vol. XXI, No. 1}
	\fancyfoot[RO,LE]{\thepage}


% ------
% Clickable URLs (optional)
%\usepackage{hyperref}

% ------
% Maketitle metadata
\title{\vspace{-15mm}%
	\fontsize{24pt}{10pt}\selectfont
	\textbf{Development of a P300 classification scheme for a multi-target somatosensory brain-computer interface}
	}	
\author{%
	\large
	\textsc{Yong Hu, Mayank Patwari} \\[2mm]
%\thanks{Template by \href{http://www.howtotex.com}{howtoTeX.com}} \\[2mm]
	\normalsize	University of Hong Kong \\
	%\normalsize	\href{mailto:frits@howtoTeX.com}{frits@howtoTeX.com}
	\vspace{-5mm}
	}
\date{}



%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\maketitle
\thispagestyle{fancy}

\begin{abstract}
\noindent The most commonly used classifiers for brain-computer interface classification are Support Vector Machines and Linear Discriminant Analysis. However, in multi-target cases the performance is not sufficient. This paper investigates the use of sliding windows and moving averages in order to create a classification scheme with electrosensory multi-target brain-computer interface data. The classifier built with sliding windows has superior performance to both Support Vector Machines and Linear Discriminant Analysis, and is significantly faster than the Support Vector Machines.
\end{abstract}
	

\begin{multicols}{2}
\noindent Literature review and introduction to be written by Dr Hu.

\section{Methods}

\textbf{\textit{Pre-processing:}} Pre-processing methods are used to reduce the amount of noise present in the signal. The first step involves bandpass filtering between 0.1Hz and 30Hz, to remove high frequency and low frequency noise. Re-referencing is done using common average reference. The data is downsampled from 1000Hz to 250Hz. Independent Component Analysis is used to decompose the EEG and remove sources of noise such as eye blinks and facial muscles. Only the electrodes in the somatosensory area with the strongest P300 are taken into consideration from this point on, which are Cz, CPz, C3, C4, POz, P1, P2, P3. The epochs are taken from 0 to 500ms after the stimulus, with 250ms before the stimulus being taken as a baseline.\\[2mm]

\noindent \textbf{\textit{Feature extraction:}} The P300 wave usually occurs at a 300ms interval, however the inter-stimulus interval in the experiment is only 250ms. Therefore a range gate of 250-500ms after the original stimulus is set to detect the P300 wave. All peaks in this region are extracted, and the peak with the largest amplitude is assumed to be the P300 wave. Two features are extracted for further consideration: the amplitude and the latency of the P300 wave. \\[2mm]

\noindent \textbf{\textit{Post-processing:}} The extracted features are taken from each of the eight electrodes, resulting in 8 sets of features per subjects. The mean of these 8 features for each epoch is calculated, ending up with a single set of features. 

\begin{equation}
Amp(n) = \frac {\sum\limits_{i=1}^{8} Amp_{i}(n)}{8}
\end{equation}

\begin{equation}
Lat(n) = \frac {\sum\limits_{i=1}^{8} Lat_{i}(n)}{8}
\end{equation}\\

\noindent where \textit{Amp} is the amplitude, \textit{Lat} is the latency, \textit{i} is the electrode number, and \textit{n} is the epoch number.\\

\noindent These features are then treated with a sliding window. The sliding window extracts 10 pairs of features at a time. The mean of these 10 pairs of features is calculated, resulting in the creation of a moving average of the features, with the length of the averaged features being the same length as the original dataset.

\begin{equation}
Amp(n) = \frac {\sum\limits_{k=0}^{9} Amp(n+k)}{10}
\end{equation}

\begin{equation}
Lat(n) = \frac {\sum\limits_{k=0}^{9} Lat(n+k)}{10}
\end{equation}\\

\noindent where \textit{Amp} is the amplitude, \textit{Lat} is the latency, and \textit{n} is the epoch number.\\[2mm]

\noindent \textbf{\textit{Classification:}} The epochs are divided into two types, target and non-target, with the former given a positive class label, and the latter given a negative class label. The class labels are combined with the set of averaged features, and used to train and subsequently test a linear discriminant classifier. There are four datasets per subject, each with a separate finger as the target. \\


\section{Results}
\textbf{\textit{Accuracy:}} Each dataset was used for training and then tested on itself. The accuracy is determined by the number of correct classifications.

\begin{equation*}
A_{classification} = \frac {C_{classifications}}{n}
\end{equation*}

\noindent where \textit{n} is the number of epochs, \textit{A\textsubscript{classification}} is the accuracy percentage, and \textit{C\textsubscript{classifications}} is the number of correct classifications.\\[2mm]
\noindent The average accuracy obtained by this method was 90.18, with a standard deviation of 1.73. The results for each subject for each target are tabulated below.

\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
  & S1 & S2& S3 & S4 & S5\\
\hline\hline
1A & 92.78 & 87.78 & 88.33 & 88.33 & 94.44\\
1B & 95.83 & 86.39 & 82.50 & 91.67 & N/A*\\
2A & 95.28 & 94.17 & 89.72 & 90.83 & 86.11\\
2B & 86.11 & 88.33 & 91.39 & 93.33 & N/A*\\
\hline
\end{tabular}
\captionof{table}{Accuracy for each subject for each stimulus.}
\end{center}

*Some data belonging to Subject 5 was corrupted, therefore only the results for two stimulus responses are shown.\\[2mm]

\noindent \textbf{\textit{Sensitivity and specificity:}} The problem is a binary classification problem, and the outputs are target and non-target. There are four possible results:

\begin{itemize}
\item True Positive: Target classified as target.
\item False Positive: Non-target classified as target.
\item False Negative: Target classified as non-target.
\item True Negative: Non-target classified as non-target.
\end{itemize}

\noindent The sensitivity and specificity are given by the followng ratios:

\begin{equation*}
Sensitivity = \frac {True Positive}{True Positive+ False Negative}
\end{equation*}

\begin{equation*}
Specificity = \frac {True Negative}{True Negative+ False Positive}
\end{equation*}\\[2mm]

\noindent The average sensitivity was 0.73 and the average specificity was 0.964. The sensitivity and specificity of each subject can be observed in the table below.

\begin{center}
\begin{tabular}{|c|c|c|}
\hline
& Sensitivity & Specificity\\
\hline\hline
S1 & 0.781 & 0.973\\
S2 & 0.672 & 0.965\\
S3 & 0.664 & 0.952\\
S4 & 0.714 & 0.976\\
S5 & 0.911 & 0.894\\
\hline
\end{tabular}
\captionof{table}{Sensitivity and specificity for each subject.}
\end{center}

\end{multicols}

\includegraphics[width = \textwidth]{flowchart}
\captionof{figure}{Flowchart of the algorithm used. The three main processes, pre-processing, feature extraction, and post-processing have been split into their various components and explained.}\pagebreak

\begin{multicols}{2}
\noindent Using the predicted scores of the classifier, ROC curves can be constructed which will help to observe the performance of the classifier. All the datasets for each subject were combined and used to create a single dataset, which was then used for both testing and training. \\[2mm]

\begin{center}
\includegraphics[width = 6.5cm]{performance}
\captionof{figure}{ ROC curve for Sliding Window for subject 1. This curve has an AUC of 0.958.}
\end{center}

\noindent  \\[2mm] The AUC is used as a metric to determine the success of the method, with an AUC of 0.9 and above considered an excellent performance. The average AUC of the 5 subjects is 0.942, with a range from 0.914 to 0.979.

\section{Discussion}
\noindent A comparison with existing classification techniques that are commonly used with BCI is necessary to observe the success of this classification scheme. The two most commonly used classifiction schemes are Linear Discriminant Analysis (LDA) and Support Vector Machines (SVM). There are two parameters which are tested for:

\begin{itemize}
\item Performance
\item Speed
\end{itemize}

\noindent First, the performance is to be compared. The performance can be compared by two metrics:

\begin{itemize}
\item Accuracy
\item Sensitivity and Specificity
\end{itemize}

\noindent The features were tested with the classifiers without any postprocessing techniques. For discussion purposes, only the features from subject 1 were included. The average accuracy with the Sliding Window method is 92.5, average with SVM is 76.11, and average with LDA is 75.21. The data can be viewed in the table below:

\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
& LDA & SVM & Sliding\\
\hline\hline
1A & 74.44 & 75.00 & 92.78\\
1B & 78.89 & 79.44 & 95.83\\
2A & 73.89 & 75.00 & 95.28\\
2B & 73.61 & 75.00 & 86.11\\
\hline
\end{tabular}
\captionof{table}{Comparison of accuracy with three classifiers for each target.}
\end{center}

\noindent  \\[2mm] The sensitivity and specificity is another important measure. The sensitivites for subject 1 are 0.155 for LDA, 0.061 for SVM, and 0.730 for Sliding Window. Similarly the specificities for subject 1 are 0.966 for LDA, 0.994 for SVM, and 0.964 for Sliding Window. There appears to be a mild inverse relation between the sensitivity and specificity. The Sliding Window has the highest sensitivity and the lowest specificity. Therefore it is not clear whether it has the best performance. To ascertain this, an ROC curve is constructed.\\[2mm]

\begin{center}
\includegraphics[width= 6.5cm]{comparison}
\captionof{figure}{ ROC curves for Sliding Window, LDA and SVM. Sliding Window has the best performance, followed by LDA and then SVM.}
\end{center}

\noindent  \\[2mm]  The AUCs are 0.958 for the Sliding Window, 0.712 for LDA, and 0.644 for SVM. This clarifies that the Sliding Window has the best classification performance among the three classifiers.\\

\noindent The speed is the second parameter to be compared. The computation time required is the only parameter. The Sliding Window requires 268ms, LDA requires 107ms, while SVM requires 6489ms. The Sliding Window has by far the best performance, and it is fast enough to use in real-time, as opposed to the SVM. The LDA is faster, however the performance is not good enough. Therefore the Sliding Window method is the best method for this type of classification.

\section{Conclusion and References}
Conclusion and references to be written by Dr Hu.

\end{multicols}

\end{document}
